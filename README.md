# 双拼助手

![输入图片说明](%E6%96%B0%E5%9B%BE%E5%83%8F.png)

#### 介绍
在屏幕上显示双拼输入法键盘布局，用于辅助学习和使用双拼输入法。

0.0.12 提升软件启动速度。

0.0.11 修复版本识别错误。

0.0.10 修复检查更新失败的错误。

0.0.9 添加软件信息和更新检查。

0.0.8 实现多种键盘布局绘制显示，支持自定义布局显示。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
